/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author speedy
 */
public class FXMLEvaluationController implements Initializable {

    
    @FXML
    private RadioButton rd_fair;

    @FXML
    private ToggleGroup radioGroup1;

    @FXML
    private RadioButton rd_poor;

    @FXML
    private Button btn_okrate;

    @FXML
    private RadioButton rd_good;

    @FXML
    private RadioButton rd_bad;

    @FXML
    private RadioButton rd_excellent;
    
    private RadioButton selected;
    
    private List<QualityListener> qualitylisteners;

    public void addQualityListener(QualityListener listener){
        this.qualitylisteners.add(listener);
    }
    
    private Stage currentstage;
    
    public void setStage(Stage stage){
        this.currentstage = stage;
    }
    
    @FXML
    void act_sendrate(ActionEvent event) {
        this.notifyQualityListener();
        this.currentstage.close();
        
    }
    
    void notifyQualityListener(){
        for(QualityListener listener : this.qualitylisteners){
            listener.qualityisset(selected);
        }
    }
    
    /**
     * Initializes the tool.controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.qualitylisteners = new ArrayList<QualityListener>();
        this.radioGroup1.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){

            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                selected = (RadioButton)newValue.getToggleGroup().getSelectedToggle();
            }
            
        });
    }    
    
    public int getResult(){
        return 0;
    }
}
