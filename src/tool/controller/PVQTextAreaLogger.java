/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.controller;

import tool.viewelements.PVQTextArea;

/**
 *
 * @author Queativ-Idea
 */
public class PVQTextAreaLogger {
    
    private static PVQTextAreaLogger logger = null;
    
    private PVQTextArea textarea;
    
    private PVQTextAreaLogger(){this.textarea = new PVQTextArea();}
    
    public static PVQTextAreaLogger getLogger(){
        if(logger == null)
            logger = new PVQTextAreaLogger();
        
        return logger;
    }
    
    public void setTextArea(PVQTextArea textarea){
        this.textarea = textarea;
    }
    
    public PVQTextArea getTextArea(){
        return this.textarea;
    }
    
    public void log(String text){
        this.textarea.appendText(text);
    }
}
