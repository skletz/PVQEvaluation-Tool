/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.DirectoryChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tool.PVQEvaluationTool;
import tool.model.Experiment;
import tool.model.Session;
import tool.viewelements.PVQTextArea;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author SabrinaKletz
 */
public class FXMLSessionController implements Initializable, ExperimentsFinishListener {
    
    private Session session = null;

    @FXML
    private Button btn_startexperiment;

    @FXML
    private Button btn_choosefile;
    
    @FXML
    private Button btn_choosereffile;    

    @FXML
    private PVQTextArea txta_feedback;
    
    
    @FXML
    private Button btn_evaluateexperiments;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        PVQTextAreaLogger.getLogger().setTextArea(this.txta_feedback);
        //create a new session - it contains a set of experiments
        this.session = new Session();
        PVQTextAreaLogger.getLogger().log("A Session was intialized; select a dataset ...");
    }
    
    
    public void setModel(Session model) {
        this.session = model;
    }
    
    private String remeberddatasetdir = null;
    private int currentdatasetid = -1;
    
    @FXML
    void act_openfilechooser(ActionEvent event) {

        List<File> files = FileDirChooser.getFileChooser("MP4 Container (*.mp4)", "*.mp4", 
                "MP4 Filechooser (Codecs: AAC and H.264)", ".").showOpenMultipleDialog(null);
 
        //if there are no files abort the action
        if (files == null) return;

        this.btn_startexperiment.setDisable(true);
        //differences the datasets
        currentdatasetid = Session.getNextDatasetId();
        
        //a session contains all experiments
        for(File file : files){
            this.session.addExperiment(new Experiment(file, currentdatasetid));
        }
        
        PVQTextAreaLogger.getLogger().log("Dataset: " + files.get(0).getParent());
        for(File file : files){
            PVQTextAreaLogger.getLogger().log("\t - " + file.getName());
        }
        
        this.remeberddatasetdir = files.get(0).getParent();
        
        PVQTextAreaLogger.getLogger().log("Choose a reference file ... ");
        this.btn_choosereffile.setDisable(false);
        this.btn_choosefile.setDisable(true);
        
        
        this.btn_evaluateexperiments.setDisable(true);
    }
    
    @FXML
    void act_openreffilechooser(ActionEvent event) {
        
        File file = FileDirChooser.getFileChooser("MP4 Container (*.mp4)", "*.mp4", 
                "MP4 Filechooser (Codecs: AAC and H.264)", this.remeberddatasetdir).showOpenDialog(null);
        
        if(file == null)
            return;
        
        this.remeberddatasetdir = null;
        
        //add the refernce file to all experiments from the current dataset
        for(Experiment exp : this.session.getExperiments()){
            if(exp.getDatasetid() == currentdatasetid)
                exp.setReferencevideofile(file);
        }
        
        this.btn_choosefile.setDisable(false);
        this.btn_choosereffile.setDisable(true);
        PVQTextAreaLogger.getLogger().log("Ready to start the experiments ... ");
        PVQTextAreaLogger.getLogger().log("OR Add further datasets ... ");
        this.btn_startexperiment.setDisable(false);
    }    

    @FXML
    void act_openexperiment(ActionEvent event) {
        try {
            Stage stage = new Stage();
            stage.setTitle("Subjective Video Quality Experiments");
            stage.initStyle(StageStyle.UNDECORATED);
            FXMLLoader loader = new FXMLLoader(PVQEvaluationTool.class.getResource("tool/view/FXMLExperiment.fxml"));
            Parent root = loader.load();
            Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
            
            Scene scene = new Scene(root, screenBounds.getWidth(), screenBounds.getHeight());
            stage.setScene(scene);
            stage.show();
            //loader.<FXMLExperimentController>getController().setExperiments(this.session.getExperiments());
            FXMLExperimentController controller = (FXMLExperimentController)loader.getController();
            //System.out.println("tool.controller: " + tool.controller);
            controller.setStage(stage);
            controller.setExperiments(this.session.getExperiments());
            controller.addExperimentFinishedListener(this);
            
            List<MediaPlayer> mediaPlayers = new ArrayList<MediaPlayer>();
            for(int i = 0; i < this.session.getExperiments().size(); i++){
                MediaPlayer player = new MediaPlayer(new Media(this.session.getExperiments().get(i).getVideoFile().toURI().toURL().toExternalForm()));
                mediaPlayers.add(player);
            }
            controller.setMediaPlayers(mediaPlayers);

            
        } catch (IOException ex) {
           Logger.getLogger(FXMLSessionController.class.getName()).log(Level.SEVERE, null, ex);
            
        }
    }
    
    
    
    
    @FXML
    void act_evaluateexperiments(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choose the folder for the result of evaluated experiments");
        chooser.setInitialDirectory(new File(System.getProperty("user.home")));
        chooser.setInitialDirectory(new File("."));
        File file = chooser.showDialog(null);
        File newfile = new File(file, "Evaluation_" + new Date(System.currentTimeMillis()).toString().replace(":", "-") + ".txt");
        try {
            newfile.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(FXMLSessionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        PVQTextAreaLogger.getLogger().log("Destination: " + newfile.getPath());
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new FileWriter(newfile));
            for(Experiment experiment : this.session.getExperiments()){
                output.write(experiment.toString());
                PVQTextAreaLogger.getLogger().log(experiment.toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(FXMLSessionController.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            if(output != null){
                try {
                    output.close();
                } catch (IOException ex) {
                    Logger.getLogger(FXMLSessionController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //this.txta_feedback.setText(this.txta_feedback.getText() + "File is written to: \n" + newfile.getPath() + "\n");
        this.txta_feedback.appendText("");
        
    }

    @Override
    public void experimentsFinished() {
        if(this.session == null)
            return;
        
        if(this.session.getExperiments().size() == 0)
            return;
        
        
        this.btn_evaluateexperiments.setDisable(false);
    }
    
}
