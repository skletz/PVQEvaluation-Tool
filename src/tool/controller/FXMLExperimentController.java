/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tool.PVQEvaluationTool;
import tool.model.Experiment;
import tool.model.Session;

/**
 *
 * @author speedy
 */
public class FXMLExperimentController implements Initializable, QualityListener {
    
    @FXML
    private MediaView v1_player;

    @FXML
    private MediaView v2_player;    
    
    @FXML
    private Button btn_player;

    @FXML
    private Button btn_next;
    
    @FXML
    private Button btn_exit;    
    
    private List<Experiment> experiments;
    
    private List<MediaPlayer> mediaPlayers;
    
    private List<ExperimentsFinishListener> experimentfinishedlistener;
    
    public void addExperimentFinishedListener(ExperimentsFinishListener listener){
        this.experimentfinishedlistener.add(listener);
    }
    
    public void notifyExperimentFinishListener(){
        for(ExperimentsFinishListener listener : this.experimentfinishedlistener){
            listener.experimentsFinished();
        }
    }
    
    private int counter = 0;
    
    private int counterofexperiments = 0;

    public void setExperiments(List<Experiment> experiments){
        this.experiments = experiments;
    }
    
    public void setMediaPlayers(List<MediaPlayer> mediaPlayers){
        this.mediaPlayers = mediaPlayers;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.experimentfinishedlistener = new ArrayList<ExperimentsFinishListener>();
        System.out.println("Initialize FXMLExperimentController");
    }
    
    private FXMLLoader dialogloader;
    
    private MediaPlayer currentPlayer;
    
    private Experiment currentExperiment;
    
    @FXML
    void act_play(ActionEvent event) {
        try {
            if(counter == 0)
                Session.addNewIteration();
            
            this.btn_player.setDisable(true);
            this.btn_exit.setDisable(true);
            System.out.println("Sample size: " + mediaPlayers.size());

            int chooserandom = randInt(0, mediaPlayers.size()-1);
            //System.out.println("Radnom sample: " + chooserandom);
            currentPlayer = this.mediaPlayers.get(chooserandom);
            currentExperiment = this.experiments.get(chooserandom);
            System.out.println("Random: " + chooserandom);
            System.out.println("Counter: " + counter);
            counter++;
        
            MediaPlayer referenceplayer = new MediaPlayer(new Media(currentExperiment.getReferencevideofile().toURI().toURL().toExternalForm()));
            this.v1_player.setMediaPlayer(referenceplayer);
            this.v2_player.setMediaPlayer(currentPlayer);
            
            currentPlayer.play();
            referenceplayer.play();
            //currentPlayer.stop();
            //currentPlayer.play();
            
            currentPlayer.setOnEndOfMedia(new Runnable() {

                @Override
                public void run() {
                    
                    //System.out.println("MediaPlayer are: " + mediaPlayers.size());
                    //System.out.println("Counter is: " + counter);
                    currentPlayer.stop();
                    referenceplayer.stop();
                    
                    if(counter == mediaPlayers.size()){
                        btn_next.setText("Start a new Experiment");
                        counter = 0;
                    }
                    openQualityDialog();
                }
            });
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLExperimentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void act_nextvideo(ActionEvent event) {

        this.v2_player.setMediaPlayer(null);
        this.v2_player.setViewport(null);
        this.v1_player.setMediaPlayer(null);
        this.v1_player.setViewport(null);    
        this.btn_player.setDisable(false);
        this.btn_next.setDisable(true);
        
        if(counter == 0){
            this.btn_next.setText("Next"); 
        }
    }
    
    
    @FXML
    void act_exit(ActionEvent event) {

        this.notifyExperimentFinishListener();
        this.currentstage.close();
    }
    
    private Stage currentstage;
    public void setStage(Stage stage){
        this.currentstage = stage;
    }
    
    void openQualityDialog(){
        try {
            this.dialogloader = new FXMLLoader(PVQEvaluationTool.class.getResource("tool/view/FXMLEvaluation.fxml"));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("Quality Evaluation");
            Parent root = dialogloader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
            
            FXMLEvaluationController controller = (FXMLEvaluationController)this.dialogloader.getController();
            controller.setStage(stage);
            controller.addQualityListener(this);
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLExperimentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void qualityisset(RadioButton selectedquality) {
        
        //@TODO change this if else
        if(selectedquality == null){
            this.currentExperiment.addRanking(3);
            this.btn_next.setDisable(false);
            this.btn_exit.setDisable(false);           
        }else{
            System.out.print("Quality is: " + selectedquality.getText());

            switch(selectedquality.getText()){
                case "Excellent":
                    this.currentExperiment.addRanking(5);
                    break;
                case "Good":
                    this.currentExperiment.addRanking(4);               
                    break;
                case "Fair":
                    this.currentExperiment.addRanking(3);
                    break;
                case "Poor":
                    this.currentExperiment.addRanking(2);
                    break;
                case "Bad":
                    this.currentExperiment.addRanking(1);
                    break;
                default:
                    throw new IllegalArgumentException("Quality is not set");
            }

            this.btn_next.setDisable(false);
            this.btn_exit.setDisable(false);
        }
    }
    
    public static int randInt(int min, int max) {
        
    Random rand = new Random();

    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
}
}
