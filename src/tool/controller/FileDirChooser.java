/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.controller;

import java.io.File;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 *
 * @author speedy
 */
public class FileDirChooser {
    
    public static FileChooser getFileChooser(String filterdescription, String filter, String title, String initDir){
        
        FileChooser chooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(filterdescription, filter);
        chooser.getExtensionFilters().add(extFilter);
        chooser.setTitle(title);
        chooser.setInitialDirectory(new File(initDir));

        return chooser;
    }
    
    public static DirectoryChooser getDirChooser(String title, String initDir){
        
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle(title);
        chooser.setInitialDirectory(new File(initDir));

        return chooser;
    }
}
