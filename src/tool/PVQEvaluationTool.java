package tool;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The tool.PVQEvaluationTool is a tool to evaluate the subjective quality of the AVC compression standard.
 * This is the main application with its primary stage. The size of the window is fixed and
 * cannot be changed. 
 * 
 * @author Sabrina Kletz
 */
public class PVQEvaluationTool extends Application {
    
    private static final String applicationname = "Perceptual Video Quality Evaluation-Tool";
    
    @Override
    public void start(Stage stage) throws Exception {
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/FXMLSession.fxml"));
        
        //change window properties
        stage.setTitle(applicationname);
        stage.centerOnScreen();
        stage.setResizable(false);
        
        Parent root = loader.load();
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
 
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
