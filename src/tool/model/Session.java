/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author speedy
 */
public class Session {
    
    private static int globaleDatasetId = 0;

    private static int iterationcounter = 0;
    
    private List<Experiment> experiments = null;
    
    public Session() {
        this.experiments = new ArrayList<Experiment>();
    }

    public List<Experiment> getExperiments() {
        return experiments;
    }

    public void setExperiments(List<Experiment> experiments) {
        this.experiments = experiments;
    }
    
    public void addExperiment(Experiment experiment){
        this.experiments.add(experiment);
    }
    
    public static int getNextDatasetId(){
        return globaleDatasetId++;
    }
    
    public static void addNewIteration(){
        iterationcounter++;
    }
    
    public static int getIterationcounter(){
        return iterationcounter;
    }
    
}
