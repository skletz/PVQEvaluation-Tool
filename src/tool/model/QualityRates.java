/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.model;

/**
 *
 * @author speedy
 */
public enum QualityRates {
    EXCELLENT(1), 
    GOOD(2), 
    FAIR(3),
    BAD(4), 
    POOR(5),
    NOTHING(-1);
    
    public final Integer value;
    
    QualityRates(Integer value){
        this.value = value;
    }

    
    

}
