/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author speedy
 */
public class Experiment {
    
    private int datasetid;
    
    private double mos = 0;
    private int sum = 0;

    public int getDatasetid() {
        return datasetid;
    }

    public void setDatasetid(int datasetid) {
        this.datasetid = datasetid;
    }
    
    private File videofile;
    private File referencevideofile;

    public File getReferencevideofile() {
        return referencevideofile;
    }

    public void setReferencevideofile(File referencevideofile) {
        this.referencevideofile = referencevideofile;
    }
    
    private List<Integer> rankings = null;

    public List<Integer> getRankings() {
        return rankings;
    }

    public Experiment(File videofile, int datasetid) {
        this.videofile = videofile;
        this.rankings = new ArrayList<Integer>();
        this.datasetid = datasetid;
    }

    public void addRanking(Integer rate){
        this.rankings.add(rate);
    }
    
    public File getVideoFile(){
        return this.videofile;
                
    }
    
    public double calculateMeanOpinionScore(){
        int sum = 0;
        
        for(Integer i : this.rankings)
            sum = sum + i;
        
        this.sum = sum;
        return (double)((double)sum / (double)this.rankings.size());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        int numberofexcellent = 0;
        int numberofgood = 0;
        int numberoffair = 0;
        int numberofpoor = 0;
        int numberofbad = 0;
        
        
        for(int rank : this.rankings){
            switch(rank){
                case 5:
                    numberofexcellent++;
                    break;
                case 4:
                    numberofgood++;
                    break;
                case 3:
                    numberoffair++;
                    break;
                case 2:
                    numberofpoor++;
                    break;
                case 1:
                    numberofbad++;
                    break;
                    
                default:
                    throw new IllegalArgumentException("Quality is not allwoed!");
            }
        }
        
        sb.append("Video File: " + videofile.getName()+ "\n");
        sb.append("\t Excellent: " + numberofexcellent + "\n");
        sb.append("\t Good: " + numberofgood + "\n");
        sb.append("\t Fair: " + numberoffair + "\n");
        sb.append("\t Poor: " + numberofpoor + "\n");
        sb.append("\t Bad: " + numberofbad + "\n");
        sb.append("\t Iterations: " + Session.getIterationcounter() + "\n");
        sb.append("\t Nr of Ratings: " + this.rankings.size() + "\n");
        sb.append("\t Sum of Ratings: " + this.sum + "\n");
        sb.append("\t MOS: " + this.calculateMeanOpinionScore() + "\n");
        
        
        return sb.toString();
    }
    
    
}
