/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tool.viewelements;

import javafx.scene.control.TextArea;

/**
 *
 * @author Queativ-Idea
 */
public class PVQTextArea extends TextArea {
    
    public void appendText(final String text){
        super.setText(super.getText() + text + "\n");
    }
    
}
