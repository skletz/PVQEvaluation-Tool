# PVQEvaluation-Tool

PVQEvaluation-Tool is a tool for evaluating the perceptual video quality in lab-based studies. 
It is not the ultimate, life-changing tool for higher studies but it may be a basis for optimization and extensions. 
The [Mean Opinion Score (MOS)](https://de.wikipedia.org/wiki/Mean_Opinion_Score) have been implemented to measure the subjective video quality.

## Why
A homework assignment! We were in search of such a program. 
Unfortunately, we haven't found a well behaved software to solve our task. 
Inspired by the [MSU Perceptual Video Quality tool](http://compression.ru/video/quality_measure/perceptual_video_quality_tool_en.html) and based on Java, a similar one have been developed.

## Changelog
Version 1.0b0
- Simple evaluation tool for video quality measurements using the MOS

## Requirementes
Tested with: Windows 7+ (64bit); Java Version 8 Update 54; 16:9 Screen Resolution

## Usage
At first you must prepare a dataset. A dataset includes at least one video content with different levels of quality and one of the quality serves as reference video. The best quality was used in our exepriments as reference.

1. Select dataset (one vidoe content with different quality levels)
2. Choose a reference file of the selected dataset
3. Then you can choose a further dataset or start the experiments
4. The comparing video qualities are selected randomly
4. After both videos have played, the participant must give a rating referring to the reference video

A cycle for one participant ends up with the enabled button "Start new experiment".

To evaluate the experiments press "Exit the experiment". Over all participants, the mean opinion score will be calculated and stored in a .txt file.

Important! If you press again "Start experiments", the previous results will not be deleted. Only the closing of the program erases all results completely.

## License
PVQEvaluation-Tool is licensed under the MIT license. For more info, read the [LICENSE](https://github.com/skletz/PVQEvaluation-Tool/blob/master/LICENSE.md) file. 